<?php

namespace app\controllers;

use app\components\{CustomController, ExtRequest, FlightResults};
use app\assets\AppAsset;
use GuzzleHttp;
use Yii;

/**
 * Class SiteController
 *
 * @package app\controllers
 */
class SiteController extends CustomController {

	/**
	 * @var ExtRequest
	 */
	private $extRequest;

	public function __construct( $id, $module, $config = [] ) {
		parent::__construct( $id, $module, $config );
		$client = new GuzzleHttp\Client();;
		$this->extRequest = new ExtRequest( $client );
	}

	/**
	 * @param $action
	 *
	 * @return mixed
	 */
	public function beforeAction( $action ) {

		$this->view->registerAssetBundle( AppAsset::className() );
		$this->initTranslations();
		$this->initYiiJSParams();
		return parent::beforeAction( $action );
	}

	/**
	 * @inheritdoc
	 */
	public function actions() {
		return [
			'error'   => [
				'class' => 'yii\web\ErrorAction',
			],
			'captcha' => [
				'class'           => 'yii\captcha\CaptchaAction',
				'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
			],
		];
	}

	/**
	 * @return mixed
	 */
	public function actionIndex() {

		return $this->render( 'index.twig', [ 'nItemsDropdowns' => 10 ] );

	}

	/**
	 * @return array
	 */
	public function actionGetflightroutes() {

		$response = [
			'response' => 'NOK',
			'message'  => '',
		];

		try {

			Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

			$response['data']     = $this->extRequest->getFlightRoutes();
			$response['response'] = 'OK';

		} catch ( \Exception $e ) {

			$response['message'] = $e->getMessage();

		}

		return $response;

	}

	public function actionGetflightschedules() {
		$response = [
			'response' => 'NOK',
			'message'  => '',
		];

		try {

			Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

			$routeFrom  = Yii::$app->request->post( 'routeFrom' );
			$routeTo    = Yii::$app->request->post( 'routeTo' );
			$flightType = Yii::$app->request->post( 'flightType' );
			$backFrom   = '';
			$backTo     = '';

			if ( ! empty( $flightType ) && $flightType === 'round-trip' ) {
				$backFrom = $routeTo;
				$backTo   = $routeFrom;
			}


			$response['data']     = $this->extRequest->getFlightSchedules( $routeFrom, $routeTo, $backFrom, $backTo );
			$response['response'] = 'OK';

		} catch ( \Exception $e ) {

			$response['message'] = $e->getMessage();

		}

		return $response;
	}

	public function actionGetflightavailability() {
		$response = [
			'response' => 'NOK',
			'message'  => '',
		];

		try {

			Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

			$routeFrom     = Yii::$app->request->post( 'routeFrom' );
			$routeTo       = Yii::$app->request->post( 'routeTo' );
			$flightType    = Yii::$app->request->post( 'flightType' );
			$dateDeparture = Yii::$app->request->post( 'dateDeparture' );
			$dateReturn    = Yii::$app->request->post( 'dateReturn' );
			$nAdults       = Yii::$app->request->post( 'nAdults' );
			$nChildren     = Yii::$app->request->post( 'nChildren' );
			$nBabies       = Yii::$app->request->post( 'nBabies' );
			$backFrom      = '';
			$backTo        = '';
			$currency      = '€';

			if ( empty( $routeFrom ) || empty( $routeTo ) || empty( $dateDeparture ) || ( $flightType !== 'one-way' && empty( $dateReturn ) ) ) {
				throw new \Exception(  Yii::t('app', 'Missing From, To, Departure Date or Return Date') );
			}

			if ( ! empty( $flightType ) && $flightType === 'round-trip' ) {
				$backFrom = $routeTo;
				$backTo   = $routeFrom;
			}

			$flightAvailabilityJSON = $this->extRequest->getFlightAvailability( $routeFrom, $routeTo, $dateDeparture, $dateReturn, $backFrom, $backTo );

			$flightResults = new FlightResults();

			$flightAvailability = $flightResults->processResults( $flightAvailabilityJSON, $routeFrom, $routeTo );


			Yii::$app->session->set( 'flightAvailability', json_encode( $flightAvailability ) );
			Yii::$app->session->set( 'nAdults', $nAdults ?? 0 );
			Yii::$app->session->set( 'nChildren', $nChildren ?? 0 );
			Yii::$app->session->set( 'nBabies',$nBabies ?? 0 );
			Yii::$app->session->set( 'flightType',$flightType ?? 'round-trip' );
			Yii::$app->session->set( 'dateDeparture',$dateDeparture );
			Yii::$app->session->set( 'dateReturn',$dateReturn );

			$response['outboundHtml'] = $this->renderPartial( 'flight-results.twig',
				[
					'resultType'    => 'OUT',
					'departureDate' => strtoupper( date('l, j F Y ', strtotime( $dateDeparture ) ) ),
					'flightList'    => $flightAvailability['OUT'],
					'currency'      => $currency,
					'destination'   => $routeTo,
					'lowerPrice'    => $flightResults->getLowerOutboundPrice()
				]
			);

			if ( ! empty( $flightType ) && $flightType === 'round-trip' ) {

				$response['returnHtml'] = $this->renderPartial( 'flight-results.twig',
					[
						'resultType'    => 'RET',
						'departureDate' => strtoupper( date('l, j F Y ', strtotime( $dateReturn ) ) ),
						'flightList'    => $flightAvailability['RET'],
						'currency'      => $currency,
						'destination'   => $routeFrom,
						'lowerPrice'    => $flightResults->getLowerReturnPrice()
					]
				);

			}

			$response['response'] = 'OK';

		} catch ( \Exception $e ) {

			$response['message'] = $e->getMessage();

		}

		return $response;
	}

	public function actionGetflightoverview() {

		$response = [
			'response' => 'NOK',
			'message'  => '',
		];

		Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

		try {

			$outFlight  = Yii::$app->request->post( 'flightOutbound' );
			$retFlight  = Yii::$app->request->post( 'flightReturn' );
			$flightType = Yii::$app->session->get( 'flightType', 'round-trip' );

			if ( empty( $outFlight ) || ( $flightType !== 'one-way' && empty( $retFlight ) ) ) {
				throw new \Exception(  Yii::t('app', 'Please select a flight') );
			}

			$retFlight = $retFlight ?? '';

			$flightResults = new FlightResults();

			$flightOverview = $flightResults->getFlightOverviewFromSession( $outFlight, $retFlight );

			$flightOverview['currency'] = '€';

			$response['overviewHtml'] = $this->renderPartial( 'flight-overview.twig', $flightOverview );
			$response['response'] = 'OK';

		} catch ( \Exception $e ) {

			$response['message'] = $e->getFile().' '.$e->getLine().' '. $e->getMessage();

		}

		return $response;

	}

}
