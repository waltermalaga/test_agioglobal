<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $css = [
        'css/font-awesome-4.7.0/css/font-awesome.min.css',
        'css/Ionicons/css/ionicons.min.css',
        'plugins/iCheck/all.css',
        'css/AdminLTE.css',
        'plugins/datepicker/css/bootstrap-datepicker.min.css',
        'plugins/sweet-alert2/sweetalert2.css',
        'css/_all-skins.min.css',
	    'css/site.css'
    ];

    public $jsOptions = ['position' => \yii\web\View::POS_HEAD];

    public $js = [
    	'js/bootstrap.min.js',
        'js/jquery.slimscroll.min.js',
        'js/fastclick.js',
        'js/jquery.blockUI.js',
        'plugins/datepicker/js/bootstrap-datepicker.min.js',
        'plugins/sweet-alert2/sweetalert2.js',
        'js/adminlte.min.js',
        'js/main.js',
    ];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
