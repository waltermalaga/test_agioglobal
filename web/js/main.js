//global selectors
var $MainSelector;
var $HeaderSelector;
var $SideBarSelector;
var $FlightRoutes;
var $AvailableDepartureDates = [];
var $AvailableReturnDates = [];

/**
 * Loader
 */
var Loader = new function () {

    this.show = function(){
        $.blockUI({ message: '<h3><i class="fa fa-refresh fa-spin fa-fw"></i>Loading...</h3>' });
    };

    this.hide = function () {

        $.unblockUI();
    };
};

/**
 * Alert
 * @constructor
 */
var Alert = new function () {

    this.error = function(message){
        sweetAlert("Error!", message, "error");
    };

    this.success = function(message){
        sweetAlert("Success!", message, 'success');
    };

    this.info = function(message){
        sweetAlert("Info", message, 'info');
    };

};

//init viewSelectors
var initViewSelectors = function(){

    var selectorId = $(document).find('.main-selector').attr('id');

    $MainSelector    = $("#"+selectorId);
    $HeaderSelector  = $("#main-header");
    $SideBarSelector = $("#left-side-bar");
};

var initDatePicker = function() {
    $MainSelector.find('#js-date-departure').datepicker({
        format: 'yyyy-mm-dd',
        startDate: new Date(),
        autoclose: true,
        constrainInput: true,
        beforeShowDay:function(date){
            var day = date.getDate() < 10 ? '0'+date.getDate() : date.getDate();
            var month = date.getMonth()+1;
            month = month <10 ? '0'+month : month;
            var ymd = date.getFullYear() +'-'+month+'-'+day;

            return $.inArray(ymd, $AvailableDepartureDates) !== -1;

        }
    }).bind("change",function(){
        var minValue = $(this).val();
        $("#js-date-return").datepicker( 'setStartDate', minValue );
    });

    $MainSelector.find('#js-date-return').datepicker({
        format: 'yyyy-mm-dd',
        startDate: new Date(),
        autoclose: true,
        beforeShowDay:function(date){
            var day = date.getDate() < 10 ? '0'+date.getDate() : date.getDate();
            var month = date.getMonth()+1;
            month = month <10 ? '0'+month : month;
            var ymd = date.getFullYear() +'-'+month+'-'+day;

           return $.inArray(ymd, $AvailableReturnDates) !== -1;
        }
    });
};

var getFlightRoutes = function() {

    Loader.show();

    $.ajax({
        'url':'/site/getflightroutes',
        'type': "POST",
        'dataType':'json',
        'data':{'csrfToken':yii_params.system.csrfToken},
        'success':function(data){

            if(data.response !== undefined && data.response == 'OK' ){

                $FlightRoutes = $.parseJSON(data.data);
                console.log($FlightRoutes);
                if ( $FlightRoutes.flightroutes ) {

                    var fromSelect = $MainSelector.find('#js-route-from');
                    $.each($FlightRoutes.flightroutes, function (index, item) {
                        if (! $('#js-route-from option[value="' +item.DepCode+ '"]').length) {
                            var htmlOption = '<option value="'+item.DepCode+'">'+item.DepCode+ ' - ' + item.DepName +'</option>';
                            fromSelect.append(htmlOption);
                        }

                    });

                } else {
                    Alert.error('No routes available');
                }

            }else if(data.message !== undefined ){

                Alert.error(data.message);

            }else{
                Alert.error(yii_params.i18n.system.GENERAL_ERROR);
            }

        },error:function(data){

            Alert.error(yii_params.i18n.system.GENERAL_ERROR);

        },complete:function(data){

            Loader.hide();
        }
    });

};

var setRouteReturnOptions = function(from) {

    var toSelect = $MainSelector.find('#js-route-to');

    toSelect.find('option')
        .remove()
        .end()
        .append('<option value="">To</option>');


    $.each($FlightRoutes.flightroutes, function (index, item) {
        if ( item.DepCode==from && !$('#js-route-to option[value="' + item.RetCode + '"]').length) {
            var htmlOption = '<option value="' + item.RetCode + '">' + item.RetCode + ' - ' + item.RetName + '</option>';
            toSelect.append(htmlOption);
        }

    });

};

var geFlightSchedules = function( routeFrom, routeTo, flightType ) {
    Loader.show();

    $.ajax({
        'url':'/site/getflightschedules',
        'type': "POST",
        'dataType':'json',
        'data':{'csrfToken':yii_params.system.csrfToken, 'routeFrom':routeFrom, 'routeTo':routeTo, 'flightType': flightType},
        'success':function(data){

            if(data.response !== undefined && data.response == 'OK' ){

                var dataResponse = $.parseJSON(data.data);
                //console.log(dataResponse.flightschedules);
                if ( dataResponse.flightschedules.OUT ) {
                    $.each(dataResponse.flightschedules.OUT, function (index, item) {
                        $AvailableDepartureDates.push(item.date);
                    });
                }

                if ( dataResponse.flightschedules.RET ) {
                    $.each(dataResponse.flightschedules.RET, function (index, item) {
                        $AvailableReturnDates.push(item.date);
                    });
                } else {
                    $("#js-date-return").val('');
                }

                initDatePicker();

            }else if(data.message !== undefined ){

                Alert.error(data.message);

            }else{
                Alert.error(yii_params.i18n.system.GENERAL_ERROR);
            }

        },error:function(data){

            Alert.error(yii_params.i18n.system.GENERAL_ERROR);

        },complete:function(data){

            Loader.hide();
        }
    });
};

var getFlightAvailability = function(routeFrom, routeTo, flightType, dateDeparture, dateReturn, nAdults, nChildren, nBabies) {
    Loader.show();

    $.ajax({
        'url':'/site/getflightavailability',
        'type': "POST",
        'dataType':'json',
        'data':{
            'csrfToken':yii_params.system.csrfToken,
            'routeFrom':routeFrom,
            'routeTo':routeTo,
            'flightType': flightType,
            'dateDeparture': dateDeparture,
            'dateReturn': dateReturn,
            'nAdults': nAdults,
            'nChildren': nChildren,
            'nBabies': nBabies
        },
        'success':function(data){

            if(data.response !== undefined && data.response == 'OK' ){

                if ( data.outboundHtml ) {
                    $MainSelector.find('.box-outbound').removeClass('hidden');
                    $MainSelector.find('#outbound-flights-container').html( data.outboundHtml );
                }

                if ( data.returnHtml ) {
                    $MainSelector.find('.box-return').removeClass('hidden');
                    $MainSelector.find('#return-flights-container').html( data.returnHtml );
                }

                $MainSelector.find('#results-container').removeClass('hidden');

                $([document.documentElement, document.body]).animate({
                    scrollTop: $("#results-container").offset().top
                }, 2000);


            }else if(data.message !== undefined ){

                Alert.error(data.message);

            }else{
                Alert.error(yii_params.i18n.system.GENERAL_ERROR);
            }

        },error:function(data){

            Alert.error(yii_params.i18n.system.GENERAL_ERROR);

        },complete:function(data){

            Loader.hide();
        }
    });
};

var getFlightOverview = function( flightOutbound, flightReturn ) {
    Loader.show();

    $.ajax({
        'url':'/site/getflightoverview',
        'type': "POST",
        'dataType':'json',
        'data':{'csrfToken':yii_params.system.csrfToken, 'flightOutbound':flightOutbound, 'flightReturn':flightReturn},
        'success':function(data){

            if(data.response !== undefined && data.response == 'OK' ){

                if ( data.overviewHtml ) {
                    $('#modal-flight-overview').find('.modal-body').html( data.overviewHtml );
                }

                $('#modal-flight-overview').modal('show');


            }else if(data.message !== undefined ){

                Alert.error(data.message);

            }else{
                Alert.error(yii_params.i18n.system.GENERAL_ERROR);
            }

        },error:function(data){

            Alert.error(yii_params.i18n.system.GENERAL_ERROR);

        },complete:function(data){

            Loader.hide();
        }
    });
};

var resetResults = function() {
    $MainSelector.find('#outbound-flights-container').html( '' );
    $MainSelector.find('#return-flights-container').html( '' );
    $MainSelector.find('#results-container').addClass('hidden');
    $MainSelector.find('.box-outbound').addClass('hidden');
    $MainSelector.find('.box-return').addClass('hidden');
};

$(function() {
    getFlightRoutes();
    initViewSelectors();

    $MainSelector
        .on('click', '#js-proceed-button', function() {
            var flightOutbound = $MainSelector.find('input[name=OUT_flight-selected]:checked').val();
            var flightReturn = $MainSelector.find('input[name=RET_flight-selected]:checked').val();
            getFlightOverview(flightOutbound,flightReturn);
        })
        .on('click', '#js-search-button', function() {
            resetResults();
            var routeFrom =  $('#js-route-from').val();
            var routeTo = $('#js-route-to').val();
            var flightType = $MainSelector.find('input[name=flight-type]:checked').val();
            var dateDeparture = $MainSelector.find('#js-date-departure').val();
            var dateReturn = $MainSelector.find('#js-date-return').val();
            var nAdults = $MainSelector.find('#js-n-adults').val();
            var nChildren = $MainSelector.find('#js-n-children').val();
            var nBabies = $MainSelector.find('#js-n-babies').val();
            
            getFlightAvailability(
                routeFrom,
                routeTo,
                flightType,
                dateDeparture,
                dateReturn,
                nAdults,
                nChildren,
                nBabies
            );
        })
        .on('change', '#js-route-from', function () {
            resetResults();
            setRouteReturnOptions($(this).val());
        })
        .on('change', '#js-route-to', function () {
            resetResults();
            var flightType = $MainSelector.find('input[name=flight-type]:checked').val();
            geFlightSchedules($('#js-route-from').val(), $(this).val(), flightType);
            $MainSelector.find('.datepicker').datepicker('destroy');
            $MainSelector.find('#js-date-departure').val('');
            $MainSelector.find('#js-date-return').val('');
        })
        .on('change', 'input[name=flight-type]', function () {
            resetResults();
            var flightType = $MainSelector.find('input[name=flight-type]:checked').val();
            var routeFrom =  $('#js-route-from').val();
            var routeTo = $('#js-route-to').val();
            if ( routeFrom.length > 0 && routeTo.length > 0 ) {
               geFlightSchedules(routeFrom, routeTo, flightType);
               $MainSelector.find('.datepicker').datepicker('destroy');
               $MainSelector.find('#js-date-departure').val('');
               $MainSelector.find('#js-date-return').val('');
            }
        });


});