<?php

$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';
require __DIR__ . '/global-constants.php';

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'defaultRoute' => 'site/index',
    'layout'=>'main.twig',
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'modules' => [

    ],
    'components' => [
        'request' => [
            'cookieValidationKey' => 'mI3d3FeID5Er9TQVKnjnqOF1j_mJ7HCA',
            'csrfParam' => 'csrfToken',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'errorHandler' => [
            //'errorAction' => 'site/error',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => $db,
        'urlManager'           => [
            'class'           => 'yii\web\UrlManager',
            'showScriptName'  => false,
            'enablePrettyUrl' => true,
            'rules'           => [
                '<controller>s'                                  => '<controller>/index',
                '<controller>/<id:\d+>'                          => '<controller>/view',
                '<controller:[\w\-]+>/<id:\d+>'                  => '<controller>/view',
                '<controller:[\w\-]+>/<action:[\w\-]+>/<id:\d+>' => '<controller>/<action>',
                '<controller:[\w\-]+>/<action:[\w\-]+>/<id:\H+>' => '<controller>/<action>',
                '<controller:[\w\-]+>/<action:[\w\-]+>'          => '<controller>/<action>',
            ]
        ],
        'view'  => [
            'class'     => 'yii\web\View',
            'renderers' => [
                'twig' => [
                    'class'      => 'yii\twig\ViewRenderer',
                    'cachePath'  => '@runtime/Twig/cache',
                    // Array of twig options:
                    'options'    => [
                        'debug'       => YII_DEBUG,
                        'auto_reload' => true,
                    ],
                    'extensions' => YII_DEBUG ? [
                        '\Twig_Extension_Debug',
                    ] : [],
                    'globals'    => [
                        'Html'     => ['class' => '\yii\helpers\Html'],
                        'Url'      => ['class' => '\yii\helpers\Url'],
                        'Pjax'     => ['class' => '\yii\widgets\Pjax'],
                        'Yii'      => ['class' => '\Yii'],
                    ],
                    'uses'       => ['yii\bootstrap'],
                    'functions'  => [
                        't'           => function ($category, $message) {
                            return \Yii::t($category, $message);
                        },
                        'encrypt'     => function ($key) {
                            return \app\components\Cipher::getInstance()->encrypt($key);
                        },
                        'decrypt'     => function ($key) {
                            return \app\components\Cipher::getInstance()->decrypt($key);
                        }

                    ]
                ],
            ],
        ]
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    /*
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        'allowedIPs' => ['127.0.0.1'],
    ];
    */
    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
}

return $config;
