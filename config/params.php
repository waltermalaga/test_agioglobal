<?php

/**
 *  Available skins
    'skin-blue',
    'skin-black',
    'skin-red',
    'skin-yellow',
    'skin-purple',
    'skin-green',
    'skin-blue-light',
    'skin-black-light',
    'skin-red-light',
    'skin-yellow-light',
    'skin-purple-light',
    'skin-green-light'
 */

return [
    'SKIN'=>'skin-blue',
    'adminEmail' => 'admin@example.com',
    'PROJECT_URL'=>strtolower(substr($_SERVER['SERVER_PROTOCOL'], 0, strpos($_SERVER['SERVER_PROTOCOL'], '/')).'s://'.$_SERVER['SERVER_NAME']),
    'DAYS_UPDATE_PASSWORD'     => 5,
    'PASSWORD-PATTERN'         =>'((?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[\W]).{8,20})',
    'MAX-LOGIN-ATTEMPS'        => 3,
    'MAX-SIZE-OLD-PASSWORDS'   => 13,
    'ACTIVATION_TOKEN_EXPIRATION'=>3600 * 24, // 1 day
    'user.passwordResetTokenExpire' => 3600,
    'user.rememberMeDuration' => 2592000 // 30 días = 3600 * 24 * 30
];
