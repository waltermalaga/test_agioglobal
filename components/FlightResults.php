<?php

namespace app\components;

use Yii;
use yii\base\Component;

class FlightResults extends Component {

	private $lowerOutboundPrice = 0;
	private $lowerReturnPrice = 0;

	/**
	 * @param array $flightInfo
	 *
	 * @return array
	 */
	private function getFlightFormattedInfo( array $flightInfo ): array {
		$duration = explode( ':', $flightInfo['duration'] );
		$addTime  = '+' . $duration[0] . ' hour +' . $duration[1] . 'minutes +' . $duration[2] . ' seconds';

		$priceFormatted = number_format( $flightInfo['price'], 2, ',', '.' );
		$price          = explode( ',', $priceFormatted );

		$flight = [
			'seatsAvailable'   => $flightInfo['seatsAvailable'],
			'priceFloat'       => number_format( $flightInfo['price'], 2, '.', '' ),
			'priceFormatted'   => $priceFormatted,
			'price'            => $price[0],
			'cents'            => $price[1],
			'departureAirport' => $flightInfo['depart']['airport']['name'],
			'arrivalAirport'   => $flightInfo['arrival']['airport']['name'],
			'timeDeparture'    => date( 'H:i', strtotime( $flightInfo['datetime'] ) ),
			'timeArrival'      => date( 'H:i', strtotime( $addTime, strtotime( $flightInfo['datetime'] ) ) )
		];

		return $flight;
	}

	/**
	 * @return float
	 */
	public function getLowerOutboundPrice() {
		return $this->lowerOutboundPrice;
	}

	/**
	 * @return float
	 */
	public function getLowerReturnPrice() {
		return $this->lowerReturnPrice;
	}

	/**
	 * @param string $results JSON with flightavailability information
	 * @param string $routeFrom
	 * @param string $routeTo
	 *
	 * @return array
	 */
	public function processResults( string $results, string $routeFrom, string $routeTo ): array {

		$resultList = json_decode( $results, true );
		$flightList = [
			'OUT' => [],
			'RET' => [],
		];

		if ( isset( $resultList['flights'] ) ) {

			if ( isset( $resultList['flights']['OUT'] ) ) {

				foreach ( $resultList['flights']['OUT'] as $result ) {
					// We only want flights going back to our origin airport
					if ( $routeFrom == $result['depart']['airport']['code'] ) {

						$flight = $this->getFlightFormattedInfo( $result );

						if ( $this->lowerOutboundPrice == 0 ) {
							$this->lowerOutboundPrice = $flight['priceFloat'];
						} else {
							$this->lowerOutboundPrice = min( $this->lowerOutboundPrice,  $flight['priceFloat'] );
						}

						if ( $routeTo != $result['arrival']['airport']['code'] ) {
							$flight['showMessage'] = 'Arrival in ' . $result['arrival']['airport']['name'];
						}

						$flightList['OUT'][] = $flight;
					}

				}

			}

			if ( isset( $resultList['flights']['RET'] ) ) {

				foreach ( $resultList['flights']['RET'] as $result ) {
					// We only want flights going back to our origin airport
					if ( $routeFrom == $result['arrival']['airport']['code'] ) {

						$flight = $this->getFlightFormattedInfo( $result );

						if ( $this->lowerReturnPrice == 0 ) {
							$this->lowerReturnPrice =  $flight['priceFloat'];
						} else {
							$this->lowerReturnPrice = min( $this->lowerReturnPrice,  $flight['priceFloat'] );
						}

						if ( $routeTo != $result['depart']['airport']['code'] ) {
							$flight['showMessage'] = 'Departure in ' . $result['depart']['airport']['name'];
						}

						$flightList['RET'][] = $flight;
					}
				}

			}

		}

		return $flightList;
	}

	/**
	 * @param string $outFlight
	 * @param string $retFlight
	 *
	 * @return array
	 * @throws \Exception
	 */
	public function getFlightOverviewFromSession( string $outFlight, string $retFlight ): array {

		$flightAvailability = Yii::$app->session->get( 'flightAvailability' );
		$nAdults            = Yii::$app->session->get( 'nAdults', 1 );
		$nChildren          = Yii::$app->session->get( 'nChildren', 0 );
		$nBabies            = Yii::$app->session->get( 'nBabies', 0 );
		$flightType         = Yii::$app->session->get( 'flightType', 'round-trip' );
		$dateDeparture      = Yii::$app->session->get( 'dateDeparture' );
		$dateReturn         = Yii::$app->session->get( 'dateReturn' );

		$resultList = json_decode( $flightAvailability, true );

		$flightOverview = [
			'departureDate' => date('D. j M Y ', strtotime( $dateDeparture ) ),
			'returnDate'    => date('D. j M Y ', strtotime( $dateReturn ) ),
			'nAdults'      => $nAdults,
			'nChildren'    => $nChildren,
			'nBabies'      => $nBabies,
			'totalPersons' => $nAdults + $nChildren + $nBabies,
			'OUT'          => [],
			'RET'          => [],
		];

		$totalPrice = 0;
		// Decrypt index of outFlight
		$indexOut = Cipher::getInstance()->decrypt( $outFlight );

		if ( array_key_exists( $indexOut - 1, $resultList['OUT'] ) ) {
			$totalPrice += $resultList['OUT'][ $indexOut - 1 ]['priceFloat'];
			$flightOverview['OUT'] = $resultList['OUT'][ $indexOut - 1 ];
		}

		if ( $flightType == 'round-trip' && ! empty( $retFlight ) ) {
			// Decrypt index of retFlight
			$indexRet = Cipher::getInstance()->decrypt( $retFlight );

			if ( array_key_exists( $indexRet - 1, $resultList['RET'] ) ) {
				$totalPrice += $resultList['RET'][ $indexRet - 1 ]['priceFloat'];
				$flightOverview['RET'] = $resultList['RET'][ $indexRet - 1 ];
			}

		}

		$flightOverview['totalPrice'] = number_format( $totalPrice * $flightOverview['totalPersons'], 2, ',', '.' );

		$price = explode( ',', $flightOverview['totalPrice'] );
		$flightOverview['total'] = $price[0];
		$flightOverview['cents'] = $price[1];


		return $flightOverview;

	}

}
