<?php

namespace app\components;

use app\assets\AppAsset;
use yii;
use yii\helpers\Json;
use yii\web\Controller;
use yii\web\AssetBundle;
use yii\web\View;

/**
 * Class CustomController
 * @package common\components
 * We will extends all own controllers from this controller.
 */
class CustomController extends Controller{

    /** @var array  */
    public $js_params = [];

    /**
     * initYiiJSParams
     * Function that allows inject variables from the controller to the  js of the view
     */
    protected function initYiiJSParams()
    {
        if ( !isset($this->js_params['i18n']) ) {
            $this->js_params['i18n'] = [];
        }

        $this->js_params['system'] = ['language' => Yii::$app->language];
        $this->js_params['system'] = ['csrfToken' => Yii::$app->request->csrfToken];
        $this->js_params['controller'] = [
            'id' => $this->id,
            'action' => $this->action->id
        ];

        $this->getView()->registerJs(
            'yii_params = ' . Json::htmlEncode($this->js_params) . ';',
            View::POS_HEAD,
            'yii-params'
        );
    }

    /**
     * initTranslations
     * Function that allows inject transalted text from the controller to the  js of the view
     */
    protected function initTranslations()
    {
        if ( !isset($this->js_params['i18n']) ) {
            $this->js_params['i18n'] = [];
        }

        $this->js_params['i18n']['system'] = [];
        $this->js_params['i18n']['system']['local_storage_not_available'] = Yii::t('app', 'this_app_cannot_run_in_private_mode');
        $this->js_params['i18n']['system']['GENERAL_ERROR'] = Yii::t('app', 'An error occurred , please try later');
        $this->js_params['i18n']['system']['LOADING_MESSAGE'] = Yii::t('app', 'Please wait...');
    }

    /**
     *
     */
    public function init()
    {
        AppAsset::register($this->getView());
        parent::init();
    }

}

?>