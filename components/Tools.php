<?php

namespace app\components;

use Yii;
use yii\base\Component;

class Tools extends Component {


	/**
	 * getMicrotime
	 *
	 * @return float
	 */
	public static function getMicrotime() {

		$milliseconds = round( microtime( true ) * 1000 );

		return $milliseconds;
	}

	/**
	 * modelErrorsToString
	 *
	 * @param $errorsArray
	 *
	 * @return string
	 */
	public static function modelErrorsToString( $errorsArray ) {

		$errors = '';

		foreach ( $errorsArray as $fieldName => $errorsList ) {

			$errors .= " " . implode( ',', $errorsList );
		}

		return $errors;

	}

	/**
	 * isValidTimeTable
	 *
	 * @param $time
	 *
	 * @return int
	 */
	public static function isValidTime( $time ) {

		$pattern = '/^([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$/';

		return preg_match( $pattern, $time );

	}

	/**
	 * renderFlashMessagesAlert
	 * We will use this method to render in the view
	 * an alert that will shown a flash message.
	 */
	public static function renderFlashMessagesAlert( $withCloseButton = true ) {

		$flashMessages = \Yii::$app->getSession()->getAllFlashes();

		$html  = '';
		$class = '';

		if ( ! empty( $flashMessages ) ) {

			$closeButton = '';

			if ( $withCloseButton ) {

				$closeButton = '<button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>';
			}

			$title = '';
			foreach ( $flashMessages as $type => $message ) {

				switch ( $type ) {

					case 'success':
						$class = 'alert-success';
						$title = '<h4><i class="icon fa fa-check"></i>' . Yii::t( 'app', 'Success' ) . '</h4>';
						break;
					case 'error':
						$class = 'alert-danger';
						$title = '<h4><i class="icon fa fa-exclamation"></i>' . Yii::t( 'app', 'Error' ) . '</h4>';
						break;
					case 'notice':
						$class = 'alert-info';
						$title = '<h4><i class="icon fa fa-info"></i>' . Yii::t( 'app', 'Info' ) . '</h4>';
						break;
				}

				$html .= '<div class="alert ' . $class . ' alert-dismissible fade in margin-bottom-0px" role="alert">
                            ' . $closeButton . '
                            ' . $title . '
                            ' . $message . '
                        </div>';

			}
		}

		echo $html . "<br>";
	}

}
