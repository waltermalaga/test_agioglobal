<?php
/**
 *  * Created by PhpStorm.
 * User: Walter Barcelos
 * Date: 09/11/2019
 * Time: 10:46
 */

namespace app\components;

use yii\base\Component;

class ExtRequest extends Component {

	const ENDPOINT = 'http://tstapi.duckdns.org/api/json/1F/';
	const USERNAME = 'php-applicant';
	const PASSWORD = 'Z7VpVEQMsXk2LCBc';
	private $client;

	public function __construct( $client ) {
		parent::__construct();

		$this->client = $client;
	}

	/**
	 * @param string $action
	 * @param array $queryVars
	 *
	 * @return mixed
	 * @throws \Exception
	 */
	private function makeRequest( string $action, array $queryVars ) {

		$url = self::ENDPOINT . $action . '?' . http_build_query( $queryVars );

		try {

			$response = $this->client->post( $url, [
				'auth' => [
					self::USERNAME,
					self::PASSWORD
				]
			] )->getBody()->getContents();

			return $response;

		} catch ( \Exception $e ) {
			throw new \Exception( $e->getMessage() );
		}

	}


	/**
	 * @param string $departureAirport
	 * @param string $destinationAirport
	 *
	 * @return mixed
	 * @throws \Exception
	 */
	public function getFlightRoutes( string $departureAirport = '', string $destinationAirport = '' ) {

		$action      = 'flightroutes';
		$queryVars = [];

		if ( ! empty( $departureAirport ) ) {
			$queryVars['departureairport'] = $departureAirport;
		}

		if ( ! empty( $destinationAirport ) ) {
			$queryVars['destinationairport'] = $destinationAirport;
		}

		return $this->makeRequest( $action, $queryVars );

	}

	/**
	 * @param string $departureAirport
	 * @param string $destinationAirport
	 * @param string $returnDepAirport
	 * @param string $returnDestAirport
	 *
	 * @return mixed
	 * @throws \Exception
	 */
	public function getFlightSchedules( string $departureAirport, string $destinationAirport, string $returnDepAirport = '', string $returnDestAirport = '' ) {

		$action      = 'flightschedules';
		$queryVars = [];

		$queryVars['departureairport']   = $departureAirport;
		$queryVars['destinationairport'] = $destinationAirport;

		if ( ! empty( $returnDepAirport ) ) {
			$queryVars['returndepartureairport'] = $returnDepAirport;
		}

		if ( ! empty( $returnDestAirport ) ) {
			$queryVars['returndestiantionairport'] = $returnDestAirport;
		}

		return $this->makeRequest( $action, $queryVars );

	}

	/**
	 * @param string $departureAirport
	 * @param string $destinationAirport
	 * @param string $departureDate
	 * @param string $returnDate
	 * @param string $returnDepAirport
	 * @param string $returnDestAirport
	 *
	 * @return mixed
	 * @throws \Exception
	 */
	public function getFlightAvailability( string $departureAirport, string $destinationAirport, string $departureDate = '', string $returnDate = '', string $returnDepAirport = '', string $returnDestAirport = '' ) {

		$action      = 'flightavailability';
		$queryVars = [];

		$queryVars['departureairport']   = $departureAirport;
		$queryVars['destinationairport'] = $destinationAirport;

		if ( ! empty( $departureDate ) ) {
			$queryVars['departuredate'] = date( 'Ymd', strtotime( $departureDate ) );
		}

		if ( ! empty( $returnDate ) ) {
			$queryVars['returndate'] = date( 'Ymd', strtotime( $returnDate ) );
		}

		if ( ! empty( $returnDepAirport ) ) {
			$queryVars['returndedepartureairport'] = $returnDepAirport;
		}

		if ( ! empty( $returnDestAirport ) ) {
			$queryVars['returndestiantionairport'] = $returnDestAirport;
		}

		return $this->makeRequest( $action, $queryVars );

	}


}