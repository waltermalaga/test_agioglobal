<?php

namespace app\components;

use Yii;
use yii\base\Exception;

/**
 * Class Cipher
 *
 * @package common\components
 *
 * @property string $encrypt_method
 * @property string $secret_key
 * @property string $secret_iv
 * @property string $key
 * @property string $iv
 */
class Cipher {
	private $encrypt_method = "AES-256-CBC";
	private $secret_key = "%2y$10Q.6w3dhOW4AEfkbH4aPHBe4pyrQs.CVQfpIvme";
	private $secret_iv = "jAGMoVMTvAyFJq";
	private $key;
	private $iv;

	/** @property Cipher $instance The reference to *Cipher* instance of this class */
	private static $instance;

	/**
	 * constructor of Cipher.
	 *
	 * @param
	 */
	public function __construct() {
		$this->key = hash( 'sha256', $this->secret_key );
		$this->iv  = substr( hash( 'sha256', $this->secret_iv ), 0, 16 );
	}

	/**
	 * Returns the *Singleton* instance of this class.
	 *
	 * @return Cipher The *Singleton* instance.
	 */
	public static function getInstance(): Cipher {
		if ( null === static::$instance ) {
			static::$instance = new static();
		}

		return static::$instance;
	}

	/**
	 * @param string $input variable to encrypt
	 *
	 * Example:
	 * Cipher::getInstance()->encrypt('mytext');
	 *
	 * @return string
	 * @throws \Exception
	 */
	public function encrypt( string $input ): string {
		$output = $input;

		try {

			if ( ! extension_loaded( 'openssl' ) ) {
				throw new Exception( 'Encryption requires the OpenSSL PHP extension' );
			}

			$output = openssl_encrypt( $input, $this->encrypt_method, $this->key, 0, $this->iv );
			$output = base64_encode( $output );
		} catch ( \Throwable $ex ) {
			throw new \Exception( $ex->getMessage() );
		}

		return $output;
	}

	/**
	 * @param string $input variable to decrypt
	 *
	 * Example:
	 * Cipher::getInstance()->decrypt('cjhwYlZ6RFdmU0dBbFdLSlBzZXZtUT09');
	 *
	 * @return string
	 * @throws \Exception
	 */
	public function decrypt( string $input ): string {
		try {
			if ( ! extension_loaded( 'openssl' ) ) {
				throw new Exception( 'Encryption requires the OpenSSL PHP extension' );
			}
			$output = openssl_decrypt( base64_decode( $input ), $this->encrypt_method, $this->key, 0, $this->iv );

		} catch ( Exception $ex ) {
			throw new \Exception( $ex->getMessage() );
		}

		return $output;
	}

}