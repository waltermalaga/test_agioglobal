TUI BE - PHP DEVELOPER - APPLICANT DEMO APPLICATION
-------------------

DIRECTORY STRUCTURE
-------------------

      assets/             contains assets definition
      commands/           contains console commands (controllers)
      components/         contains our custom components
      config/             contains application configurations
      controllers/        contains Web controller classes
      mail/               contains view files for e-mails
      models/             contains model classes
      runtime/            contains files generated during runtime
      tests/              contains various tests for the basic application
      vendor/             contains dependent 3rd-party packages
      views/              contains view files for the Web application
      web/                contains the entry script and Web resources



REQUIREMENTS
------------

The minimum requirement is that your Web server supports PHP 7.x

OVERVIEW
------------

For this project, I'm using the MVC framework [Yii2](https://www.yiiframework.com/).

The frontend is based on [AdminLTE2](https://adminlte.io/themes/AdminLTE/index2.html) template, with Bootstrap 3 and jQuery.

The PHP was coded by using PHPStorm running PHP 7.2.1 locally. Twig is used as template engine.

For the external API connections, I'm using [Guzzle](http://docs.guzzlephp.org/en/stable/index.html), but it could be easily replaced by a simple cURL from PHP.


VIRTUAL HOST
------------

In order to run this demo, point your virtual host to your ROOT/web folder

MAIN CLASSES
------------

 - *controllers/SiteController.php* is the only controller of the project.
 - *components/Cipher.php* uses openssl to encrypt and decrypt information.
 - *components/CustomController.php* defines vars and translation texts so we can use them in Javascript if needed.
 - *components/ExtRequest.php* manages the API requests.
 - *components/FlightResults.php* transforms the API request results so we can send the information formatted to the frontend.

CONSIDERATIONS
------------

- For the calendars, I'm using the bootstrap calendar component. 
- I defined some custom Twig functions, like *encrypt* and *decrypt*, which uses a component named *Cipher*. It requires openssl extension. 
- When first loading the page, I request the flight routes so the *From* and *To* dropdowns get filled.
- Once the user selects the *From* and *To* airports, I do an API call by ajax to *flightschedule* so I insert the allowed dates in the calendars *Departure* and *Return*.
- The start date of *Departure* calendar is controlled so we don't get past dates.
- Once the user selects the *Departure* date, the *Return* dates are being dynamically calculated taking in account the selected *Departure* date and the *To* airport, so we only show the allowed return dates.

